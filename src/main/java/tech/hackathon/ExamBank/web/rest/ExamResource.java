package tech.hackathon.ExamBank.web.rest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import tech.hackathon.ExamBank.service.ExamService;
import tech.hackathon.ExamBank.service.dto.SchoolDTO;

@RestController
@RequestMapping("/api")
@Slf4j
public class ExamResource {
    private final ExamService examService;
    public ExamResource(ExamService examService) {
        this.examService = examService;
    }
    //save paper
    @PostMapping("/save-paper")
    public SchoolDTO savePaper(@RequestBody SchoolDTO schoolDTO){
        log.debug("Request to save past paper {}", schoolDTO);
        return examService.savePaper(schoolDTO);
    }

    //find by code
//    @GetMapping("/find-by-code/{unitCode}")
//    public Exam findByCode(@PathVariable String unitCode){
//        log.debug("Request to find exam paper by code{}", unitCode);
//
//        return examService.findOneByCode(unitCode);
//    }
   // @GetMapping("/find-by-school/{school}")
    //List all papers by school
//    public Iterable<Exam> findBySchool(@PathVariable String school){
//        log.debug("Request to list all papers in school {}", school);
//        return examService.findExams(school);
//    }
}
