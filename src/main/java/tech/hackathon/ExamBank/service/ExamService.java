package tech.hackathon.ExamBank.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tech.hackathon.ExamBank.configs.ApplicationProperties;
import tech.hackathon.ExamBank.repository.ExamRepository;
import tech.hackathon.ExamBank.service.dto.SchoolDTO;
import tech.hackathon.ExamBank.service.mapper.PaperMapper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service
@Slf4j
public class ExamService {
    private final ExamRepository examRepository;
    private final PaperMapper paperMapper;
    private final ApplicationProperties applicationProperties;

    public ExamService(ExamRepository examRepository, PaperMapper paperMapper, ApplicationProperties applicationProperties) {
        this.examRepository = examRepository;
        this.paperMapper = paperMapper;
        this.applicationProperties = applicationProperties;
    }
    //save paper in a school
    public SchoolDTO savePaper(SchoolDTO schoolDTO){
        //set school folder
        log.debug("Request to set school folder...");
      //  schoolDTO.setSchoolName(UUID.randomUUID().toString());
        log.debug("Request to save info");
        examRepository.save(paperMapper.toEntity(schoolDTO));

        //create a folder
        try {
            Path path = Paths.get(applicationProperties.getSchoolFolder() + "/" +schoolDTO.getSchoolName()+"/"+schoolDTO.getPaperDTO().getYearOfStudy());
            Files.createDirectories(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return schoolDTO;
    }
}
