package tech.hackathon.ExamBank.service.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import tech.hackathon.ExamBank.domain.School;
import tech.hackathon.ExamBank.service.dto.PaperDTO;
import tech.hackathon.ExamBank.service.dto.SchoolDTO;

@Service
public class PaperMapper {
    private final ObjectMapper objectMapper = new ObjectMapper();

    //convert into an entity
    public School toEntity(SchoolDTO schoolDTO){
        if (schoolDTO == null){
            return null;
        }
        //create an instance of school
        School school = new School();
        school.setId(schoolDTO.getId());
        school.setSchoolName(schoolDTO.getSchoolName());

        try {
            school.setPaperInfo(objectMapper.writeValueAsString(schoolDTO.getPaperDTO()));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return school;
    }
    public SchoolDTO toDTO(School school){
        if (school == null){
            return null;
        }
        SchoolDTO schoolDTO = new SchoolDTO();
        schoolDTO.setId(school.getId());
        schoolDTO.setSchoolName(school.getSchoolName());
        try {
            schoolDTO.setPaperDTO(objectMapper.readValue(school.getPaperInfo(), new TypeReference<PaperDTO>() {
            }));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return schoolDTO;
    }
}
