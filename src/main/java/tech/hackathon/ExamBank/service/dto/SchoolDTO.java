package tech.hackathon.ExamBank.service.dto;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
public class SchoolDTO {
    private Long id;
    private String schoolName;
    private PaperDTO paperDTO;
}
