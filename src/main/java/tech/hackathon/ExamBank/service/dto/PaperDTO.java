package tech.hackathon.ExamBank.service.dto;

import lombok.Data;
import tech.hackathon.ExamBank.domain.School;

@Data
public class PaperDTO {
    //private int id;
    //private String school;
    private String yearOfStudy;
    private String unitCode;
    private int examYear;
    private String unitName;

}
