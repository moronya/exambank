package tech.hackathon.ExamBank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import tech.hackathon.ExamBank.configs.ApplicationProperties;


@SpringBootApplication
@EnableConfigurationProperties(ApplicationProperties.class)
public class ExamBankApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamBankApplication.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/api/**").allowedOrigins("*").allowedMethods("*");
			}
		};

	}
}