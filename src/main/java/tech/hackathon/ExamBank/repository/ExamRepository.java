package tech.hackathon.ExamBank.repository;


import org.springframework.data.repository.CrudRepository;
import tech.hackathon.ExamBank.domain.School;

import java.util.Optional;

public interface ExamRepository extends CrudRepository<School, Long> {
   // Optional<Exam> findByUnitCodeIgnoreCase(String unitCode);
   // Iterable<Exam> findBySchool(String school);
}
