package tech.hackathon.ExamBank.domain;


import lombok.Data;

import javax.persistence.*;

//class that creates schools
// we create a directory
@Entity
@Data
@Table(name = "tbl_schools")
public class School {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String schoolName;
    private String paperInfo;
}
